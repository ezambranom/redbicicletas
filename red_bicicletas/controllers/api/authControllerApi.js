var Usuario = require("../../models/usuario");
var bcrypt = require("bcrypt");
var jwt = require("jsonwebtoken");

module.exports = {
    authenticate: function (req, res, next) {
        Usuario.findOne({ email: req.body.email }, function (err, usuario) {
            if (!err) {
                next(err);
            }
            else {
                if (usuario === null) return res.status(401).json({ status: "error", message: "Email o password invalido.", data: null });

                if (usuario != null && bcrypt.compareSync(req.body.password, usuario.password)) {
                    var token = jwt.sign({ id: usuario._id }, req.app.get("secretKey"), { expiresIn: "7d" });
                    res.status(200).json({ message: "Usuario encontrado.", data: { usuario: usuario, token: token } });
                }
                else {
                    res.status(401).json({ status: "error", message: "Email o password invalido.", data: null });
                }
            }
        })
    },

    forgotPassword: function (req, res, next) {
        Usuario.findOne({ email: req.body.email }, function (err, usuario) {
            if (!usuario) return res.status(401).json({ message: "No existe el usuario.", data: null });

            usuario.resetPassword(function (err) {
                if (err) return next(err);

                res.status(200).json({ message: "Se envio un email para reestablecer el password.", data: null });
            });
        });
    },

    authFacebookToken: function (req, res, next) {
        if (req.user) {
            req.user.save().then(() => {
                const token = jwt.sign({ id: req.user.id}, req.app.get("secretKey"), { expiresIn: "7d" });
                res.status(200).json({ message: "Usuario encontrado.", data: { user: req.user, token: token } });
            }).catch((err) => {
                console.log(err);
                res.status(500).json({ message: err.message });
            });
        }
        else {
            res.status(401);
        }
    }
};