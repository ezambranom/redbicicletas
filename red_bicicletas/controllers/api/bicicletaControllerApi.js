var Bicicleta = require("../../models/bicicleta");

exports.bicicleta_list = function (req, res) {
    res.status(200).json({
        bicicletas: Bicicleta.allBicis
    });
}

exports.bicicleta_create = function (req, res) {
    var bici = new Bicicleta({ codigo: req.body.codigo, color: req.body.color, modelo: req.body.modelo });
    bici.ubicacion = [req.body.lat, req.body.lng];
    Bicicleta.add(bici);

    res.status(200).json({ bicicleta: bici });
}

exports.bicicleta_update = function(req, res){
    var bici = Bicicleta.findByCode(req.body.codigo);
    bici.codigo = req.body.codigo;
    bici.color = req.body.color;
    bici.modelo = req.body.modelo;
    bici.ubicacion = [req.body.lat,req.body.lng];

    res.status(200).json({
        bicicleta:bici
    });
}

exports.bicicleta_delete = function (req, res) {
    Bicicleta.removeByCode(req.body.codigo);

    res.status(204).send();
}