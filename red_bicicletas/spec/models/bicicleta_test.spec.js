var mongoose = require("mongoose");
var Bicicleta = require('../../models/bicicleta');

describe("Testing Bicicletas", function(){
    beforeEach(function(done){
        var mongoDB = "mongodb://localhost/testdb";
        mongoose.connect(mongoDB, { useNewUrlParser: true, useUnifiedTopology: true });

        const db = mongoose.connection;
        db.on("error", console.error.bind(console, "Connection error."));
        db.on("open", function(){
            console.log("We are connected to test database.");
            done();
        });
    });

    afterEach(function(done){
        Bicicleta.deleteMany({}, function(err, success){
            if (err) console.log(err);
            done();
        });
    });

    describe("Bicicleta.createInstance", () => {
        it("crear una instancia", () => {
            var b = Bicicleta.createInstance(1, "rojo", "urbana", [-34.5, -54.1]);

            expect(b.codigo).toBe(1);
            expect(b.color).toBe("rojo");
            expect(b.modelo).toBe("urbana");
            expect(b.ubicacion[0]).toBe(-34.5);
            expect(b.ubicacion[1]).toBe(-54.1);
        });
    });

    describe('Bicicleta.allBicis', () => {
        it('comienza vacia', (done) => {
            Bicicleta.allBicis(function(err, b){
                expect(b.length).toBe(0);
                done();
            });
        });
    });

    describe("Bicicleta.add", () => {
        it("agregar una bicicleta", (done) => {
            var b = new Bicicleta({ codigo: 1, color: "verde", modelo: "urbana" });
            Bicicleta.add(b, function(err, newBici){
                if (err) console.log(err);
                Bicicleta.allBicis(function(err, bicis){
                    expect(bicis.length).toEqual(1);
                    expect(bicis[0].codigo).toEqual(b.codigo);
                    done();
                });
            });
        });
    });

    describe("Bicicleta.findByCode", () => {
        it("buscando la bici con código 1", (done) => {
            Bicicleta.allBicis(function(err, bicis){
                expect(bicis.length).toBe(0);

                var b1 = new Bicicleta({ codigo: 1, color: "verde", modelo: "urbana" });
                Bicicleta.add(b1, function(err, newBici){
                    if (err) console.log(err);

                    var b2 = new Bicicleta({ codigo: 2, color: "roja", modelo: "urbana" });
                    Bicicleta.add(b2, function(err, newBici){
                        if (err) console.log(err);

                        Bicicleta.findByCode(1, function(error, targetBici){
                            expect(targetBici.codigo).toBe(b1.codigo);
                            expect(targetBici.color).toBe(b1.color);
                            expect(targetBici.modelo).toBe(b1.modelo);

                            done();
                        });
                    });
                });
    
            });
        });
    });

});
// beforeEach(() => { Bicicleta.allBicis = []; });

// describe('Bicicleta.allBicis', () => {
//     it('comienza vacia', () => {
//         expect(Bicicleta.allBicis.length).toBe(0);
//     });
// });

// describe('Bicicleta.add', () => {
//     it('agregamos una', () => {
//         expect(Bicicleta.allBicis.length).toBe(0);

//         var a = new Bicicleta(1, "rojo", "urbana", [-34.6012424,-58.3861497]);
//         Bicicleta.add(a);

//         expect(Bicicleta.allBicis.length).toBe(1);
//         expect(Bicicleta.allBicis[0]).toBe(a);
//     });
// });

// describe('Bicicleta.findById', () => {
//     it('Debe devolver la bici con id 1', () => {
//         expect(Bicicleta.allBicis.length).toBe(0);

//         var b1 = new Bicicleta(1, "blanca", "urbana");
//         var b2 = new Bicicleta(2, "verde", "montaña");

//         Bicicleta.add(b1);
//         Bicicleta.add(b2);

//         var targetBici = Bicicleta.findById(1);
//         expect(targetBici.id).toBe(1);
//         expect(targetBici.color).toBe(b1.color);
//         expect(targetBici.modelo).toBe(b1.modelo);
//     });
// });