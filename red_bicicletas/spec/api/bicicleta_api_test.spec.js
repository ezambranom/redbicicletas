var Bicicleta = require("../../models/bicicleta");
var request = require("request");
var server = require("../../bin/www");

var base_url = "http://localhost:3000/api/bicicletas";

describe("Bicicleta API", () => {
    beforeEach(function(done){
        var mongoDB = "mongodb://localhost/testdb";
        mongoose.connect(mongoDB, { useNewUrlParser: true, useUnifiedTopology: true });

        const db = mongoose.connection;
        db.on("error", console.error.bind(console, "Connection error."));
        db.on("open", function(){
            console.log("We are connected to test database.");
            done();
        });
    });

    afterEach(function(done){
        Bicicleta.deleteMany({}, function(err, success){
            if (err) console.log(err);
            done();
        });
    });

    describe("GET Bicicletas", () => {
        it("Status 200", (done) => {
            request.get(base_url, function(error, response, body){
                var result = JSON.parse(body);
                expect(response.statusCode).toBe(200);
                expect(result.bicicletas.length).toBe(0);
                done();
            });
        });
    });

    describe("POST Bicicletas create", () => {
        it("Status 200", (done) => {
            var headers = { "content-type": "application/json" };
            var a = '{ "codigo": 2, "color": "amarillo", "modelo": "montaña", "lat": -34, "lng": -54 }';

            request.post({
                headers: headers,
                url: base_url + "/create",
                body: a
            }, function(error, response, body){
                expect(response.statusCode).toBe(200);
                var bici = JSON.parse(body).bicicleta;
                console.log(bici);
                expect(bici.color).toBe("amarillo");
                expect(bici.ubicacion[0]).toBe(-34);
                expect(bici.ubicacion[1]).toBe(-54);
                done();
            });
        });
    });

    describe("POST Bicicletas update", () => {
        it("Status 200", (done) => {
            var headers = { "content-type": "application/json" };
            var aBici = '{ "codigo": 10, "color": "verde", "modelo": "montaña", "lat": -34, "lng": -54}';

            var a = Bicicleta.createInstance(10, "verde", "montaña", [-34, -54]);
            Bicicleta.add(a);

            request.post({
                headers: headers,
                url: base_url + "/update",
                body: aBici
            }, function(error, response,body){
                expect(response.statusCode).toBe(200);
                var bici = JSON.parse(body).bicicleta;
                console.log(bici);
                expect(bici.color).toBe("verde");
                expect(bici.ubicacion[0]).toBe(-34);
                expect(bici.ubicacion[1]).toBe(-54);
                done();
            });
        });
    });
        
    describe("POST Bicicletas delete", () => {
        it("Status 200", (done) => {
            var headers = { "content-type": "application/json" };
            var aBici = '{ "codigo": 3 }';

            var a = Bicicleta.createInstance(3, "negro", "urbana", [-34, -54]);
            Bicicleta.add(a);

            request.delete({
                headers: headers,
                url: base_url + "/delete",
                body: aBici
            }, function(error, response,body){
                expect(response.statusCode).toBe(204);
                done();
            });
        });
    });

});
