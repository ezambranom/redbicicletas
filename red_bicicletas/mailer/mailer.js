const nodeMailer = require("nodemailer");
const sgTransport = require("nodemailer-sendgrid-transport");

let mailConfig;
if (process.env.NODE_ENV === 'production') {
    const options = {
        auth: {
            api_key: process.env.SENDGRID_API_SECRET
        }
    };
    mailConfig = sgTransport(options);
}
else {
    if (process.env.NODE_ENV === 'staging') {
        const options = {
            auth: {
                api_key: process.env.SENDGRID_API_SECRET
            }
        };
        mailConfig = sgTransport(options);
    }
    else {
        mailConfig = {
            host: "smtp.ethereal.email",
            port: 587,
            auth: {
                user: "ross.schmidt@ethereal.email",
                pass: "FFVgwzww1xyQq6Maa6"
            }
        };
    }
}

module.exports = nodeMailer.createTransport(mailConfig);