var mongoose = require("mongoose");
var Schema = mongoose.Schema;

var bicicletaSchema = new Schema({
    codigo: Number,
    color: String,
    modelo: String,
    ubicacion: {
        type: [Number], index: { type: "2dsphere", sparse: true }
    }
});

bicicletaSchema.statics.createInstance = function (codigo, color, modelo, ubicacion) {
    return new this({
        codigo: codigo,
        color: color,
        modelo: modelo,
        ubicacion: ubicacion
    });
}

bicicletaSchema.methods.toString = function () {
    return "codigo: " + this.codigo + " | color: " + this.color;
}

bicicletaSchema.statics.allBicis = function(cb){
    return this.find({}, cb);
};

bicicletaSchema.statics.add = function(bici, cb){
    this.create(bici, cb);
};

bicicletaSchema.statics.findByCode = function(code, cb){
    return this.findOne({ codigo: code }, cb);
};

bicicletaSchema.statics.removeByCode = function(code, cb){
    return this.deleteOne({ codigo: code }, cb);
};

module.exports = mongoose.model("Bicicleta", bicicletaSchema);

// var Bicicleta = function (id, color, modelo, ubicacion) {
//     this.id = id;
//     this.color = color;
//     this.modelo = modelo;
//     this.ubicacion = ubicacion;
// }

// Bicicleta.prototype.toString = function () {
//     return "id: " + this.id + " | color: " + this.color;
// }

// Bicicleta.allBicis = [];

// Bicicleta.add = function (aBici) {
//     Bicicleta.allBicis.push(aBici);
// }

// Bicicleta.findById = function (aBiciId) {
//     var aBici = Bicicleta.allBicis.find(r => r.id == aBiciId);

//     if (aBici) {
//         return aBici;
//     }
//     else {
//         throw new Error(`No existe una bicicleta con el id ${aBiciId}`);
//     }
// }

// Bicicleta.removeById = function (aBiciId) {
//     for (var i = 0; i < Bicicleta.allBicis.length; i++) {
//         if (Bicicleta.allBicis[i].id == aBiciId) {
//             Bicicleta.allBicis.splice(i, 1);
//             break;
//         }
//     }
// }

// // var a = new Bicicleta(1, "rojo", "urbana", [-34.6012424,-58.3861497]);
// // var b = new Bicicleta(2, "blanca", "urbana", [-34.596932,-58.3808287]);

// // Bicicleta.add(a);
// // // Bicicleta.add(b);

// module.exports = Bicicleta;